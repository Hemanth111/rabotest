package com.example.rabotest;

import com.example.rabotest.rest.Stubs;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication public class RabotestApplication {

    public static Stubs stubs = new Stubs();

    public static void main(String[] args) {
        stubs.setUp()
            .stubForGetEmployeeLogin()
            .status();

        SpringApplication.run(RabotestApplication.class, args);

    }

}
