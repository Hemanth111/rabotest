package com.example.rabotest;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.rabotest.rest.EmployeeController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest class RabotestApplicationTests {

    @Autowired private EmployeeController controller;

    @Test void contextLoads() {
        assertThat(controller).isNotNull();
    }

}
