package com.example.rabotest.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller public class EmployeeController {
    @GetMapping(path = "/rabotest") ResponseEntity<String> employee() {
        return ResponseEntity.ok("Hello, all API tests were successful.");
    }
}
